/*
 * Copyright (c) 2023, Alliance for Open Media. All rights reserved
 *
 * This source code is subject to the terms of the BSD 2 Clause License and
 * the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
 * was not distributed with this source code in the LICENSE file, you can
 * obtain it at www.aomedia.org/license/software. If the Alliance for Open
 * Media Patent License 1.0 was not distributed with this source code in the
 * PATENTS file, you can obtain it at www.aomedia.org/license/patent.
 */

#include "av1/encoder/encoder.h"
#include "av1/encoder/encoder_utils.h"
#include "av1/encoder/firstpass.h"
#include "av1/encoder/rdopt.h"
#include "av1/encoder/saliency_map.h"

// The Gabor filter is generated by setting the parameters as:
// ksize = 9
// sigma = 1
// theta = y*np.pi/4, where y /in {0, 1, 2, 3}, i.e., 0, 45, 90, 135 degree
// lambda1 = 1
// gamma=0.8
// phi =0
static const double kGaborFilter[4][9][9] = {  // [angle: 0, 45, 90, 135
                                               // degree][ksize][ksize]
  { { 2.0047323e-06, 6.6387620e-05, 8.0876675e-04, 3.6246411e-03, 5.9760227e-03,
      3.6246411e-03, 8.0876675e-04, 6.6387620e-05, 2.0047323e-06 },
    { 1.8831115e-05, 6.2360091e-04, 7.5970138e-03, 3.4047455e-02, 5.6134764e-02,
      3.4047455e-02, 7.5970138e-03, 6.2360091e-04, 1.8831115e-05 },
    { 9.3271126e-05, 3.0887155e-03, 3.7628256e-02, 1.6863814e-01, 2.7803731e-01,
      1.6863814e-01, 3.7628256e-02, 3.0887155e-03, 9.3271126e-05 },
    { 2.4359586e-04, 8.0667874e-03, 9.8273583e-02, 4.4043165e-01, 7.2614902e-01,
      4.4043165e-01, 9.8273583e-02, 8.0667874e-03, 2.4359586e-04 },
    { 3.3546262e-04, 1.1108996e-02, 1.3533528e-01, 6.0653067e-01, 1.0000000e+00,
      6.0653067e-01, 1.3533528e-01, 1.1108996e-02, 3.3546262e-04 },
    { 2.4359586e-04, 8.0667874e-03, 9.8273583e-02, 4.4043165e-01, 7.2614902e-01,
      4.4043165e-01, 9.8273583e-02, 8.0667874e-03, 2.4359586e-04 },
    { 9.3271126e-05, 3.0887155e-03, 3.7628256e-02, 1.6863814e-01, 2.7803731e-01,
      1.6863814e-01, 3.7628256e-02, 3.0887155e-03, 9.3271126e-05 },
    { 1.8831115e-05, 6.2360091e-04, 7.5970138e-03, 3.4047455e-02, 5.6134764e-02,
      3.4047455e-02, 7.5970138e-03, 6.2360091e-04, 1.8831115e-05 },
    { 2.0047323e-06, 6.6387620e-05, 8.0876675e-04, 3.6246411e-03, 5.9760227e-03,
      3.6246411e-03, 8.0876675e-04, 6.6387620e-05, 2.0047323e-06 } },

  { { -6.2165498e-08, 3.8760313e-06, 3.0079011e-06, -4.4602581e-04,
      6.6981313e-04, 1.3962291e-03, -9.9486928e-04, -8.1631159e-05,
      3.5712848e-05 },
    { 3.8760313e-06, 5.7044272e-06, -1.6041942e-03, 4.5687673e-03,
      1.8061366e-02, -2.4406660e-02, -3.7979286e-03, 3.1511115e-03,
      -8.1631159e-05 },
    { 3.0079011e-06, -1.6041942e-03, 8.6645801e-03, 6.4960226e-02,
      -1.6647682e-01, -4.9129307e-02, 7.7304743e-02, -3.7979286e-03,
      -9.9486928e-04 },
    { -4.4602581e-04, 4.5687673e-03, 6.4960226e-02, -3.1572008e-01,
      -1.7670043e-01, 5.2729243e-01, -4.9129307e-02, -2.4406660e-02,
      1.3962291e-03 },
    { 6.6981313e-04, 1.8061366e-02, -1.6647682e-01, -1.7670043e-01,
      1.0000000e+00, -1.7670043e-01, -1.6647682e-01, 1.8061366e-02,
      6.6981313e-04 },
    { 1.3962291e-03, -2.4406660e-02, -4.9129307e-02, 5.2729243e-01,
      -1.7670043e-01, -3.1572008e-01, 6.4960226e-02, 4.5687673e-03,
      -4.4602581e-04 },
    { -9.9486928e-04, -3.7979286e-03, 7.7304743e-02, -4.9129307e-02,
      -1.6647682e-01, 6.4960226e-02, 8.6645801e-03, -1.6041942e-03,
      3.0079011e-06 },
    { -8.1631159e-05, 3.1511115e-03, -3.7979286e-03, -2.4406660e-02,
      1.8061366e-02, 4.5687673e-03, -1.6041942e-03, 5.7044272e-06,
      3.8760313e-06 },
    { 3.5712848e-05, -8.1631159e-05, -9.9486928e-04, 1.3962291e-03,
      6.6981313e-04, -4.4602581e-04, 3.0079011e-06, 3.8760313e-06,
      -6.2165498e-08 } },

  { { 2.0047323e-06, 1.8831115e-05, 9.3271126e-05, 2.4359586e-04, 3.3546262e-04,
      2.4359586e-04, 9.3271126e-05, 1.8831115e-05, 2.0047323e-06 },
    { 6.6387620e-05, 6.2360091e-04, 3.0887155e-03, 8.0667874e-03, 1.1108996e-02,
      8.0667874e-03, 3.0887155e-03, 6.2360091e-04, 6.6387620e-05 },
    { 8.0876675e-04, 7.5970138e-03, 3.7628256e-02, 9.8273583e-02, 1.3533528e-01,
      9.8273583e-02, 3.7628256e-02, 7.5970138e-03, 8.0876675e-04 },
    { 3.6246411e-03, 3.4047455e-02, 1.6863814e-01, 4.4043165e-01, 6.0653067e-01,
      4.4043165e-01, 1.6863814e-01, 3.4047455e-02, 3.6246411e-03 },
    { 5.9760227e-03, 5.6134764e-02, 2.7803731e-01, 7.2614902e-01, 1.0000000e+00,
      7.2614902e-01, 2.7803731e-01, 5.6134764e-02, 5.9760227e-03 },
    { 3.6246411e-03, 3.4047455e-02, 1.6863814e-01, 4.4043165e-01, 6.0653067e-01,
      4.4043165e-01, 1.6863814e-01, 3.4047455e-02, 3.6246411e-03 },
    { 8.0876675e-04, 7.5970138e-03, 3.7628256e-02, 9.8273583e-02, 1.3533528e-01,
      9.8273583e-02, 3.7628256e-02, 7.5970138e-03, 8.0876675e-04 },
    { 6.6387620e-05, 6.2360091e-04, 3.0887155e-03, 8.0667874e-03, 1.1108996e-02,
      8.0667874e-03, 3.0887155e-03, 6.2360091e-04, 6.6387620e-05 },
    { 2.0047323e-06, 1.8831115e-05, 9.3271126e-05, 2.4359586e-04, 3.3546262e-04,
      2.4359586e-04, 9.3271126e-05, 1.8831115e-05, 2.0047323e-06 } },

  { { 3.5712848e-05, -8.1631159e-05, -9.9486928e-04, 1.3962291e-03,
      6.6981313e-04, -4.4602581e-04, 3.0079011e-06, 3.8760313e-06,
      -6.2165498e-08 },
    { -8.1631159e-05, 3.1511115e-03, -3.7979286e-03, -2.4406660e-02,
      1.8061366e-02, 4.5687673e-03, -1.6041942e-03, 5.7044272e-06,
      3.8760313e-06 },
    { -9.9486928e-04, -3.7979286e-03, 7.7304743e-02, -4.9129307e-02,
      -1.6647682e-01, 6.4960226e-02, 8.6645801e-03, -1.6041942e-03,
      3.0079011e-06 },
    { 1.3962291e-03, -2.4406660e-02, -4.9129307e-02, 5.2729243e-01,
      -1.7670043e-01, -3.1572008e-01, 6.4960226e-02, 4.5687673e-03,
      -4.4602581e-04 },
    { 6.6981313e-04, 1.8061366e-02, -1.6647682e-01, -1.7670043e-01,
      1.0000000e+00, -1.7670043e-01, -1.6647682e-01, 1.8061366e-02,
      6.6981313e-04 },
    { -4.4602581e-04, 4.5687673e-03, 6.4960226e-02, -3.1572008e-01,
      -1.7670043e-01, 5.2729243e-01, -4.9129307e-02, -2.4406660e-02,
      1.3962291e-03 },
    { 3.0079011e-06, -1.6041942e-03, 8.6645801e-03, 6.4960226e-02,
      -1.6647682e-01, -4.9129307e-02, 7.7304743e-02, -3.7979286e-03,
      -9.9486928e-04 },
    { 3.8760313e-06, 5.7044272e-06, -1.6041942e-03, 4.5687673e-03,
      1.8061366e-02, -2.4406660e-02, -3.7979286e-03, 3.1511115e-03,
      -8.1631159e-05 },
    { -6.2165498e-08, 3.8760313e-06, 3.0079011e-06, -4.4602581e-04,
      6.6981313e-04, 1.3962291e-03, -9.9486928e-04, -8.1631159e-05,
      3.5712848e-05 } }
};

// This function is to extract red/green/blue channels, and calculate intensity
// = (r+g+b)/3. Note that it only handles 8bits case now.
// TODO(linzhen): add high bitdepth support.
static void get_color_intensity(YV12_BUFFER_CONFIG *src, int subsampling_x,
                                int subsampling_y, double *Cr, double *Cg,
                                double *Cb, double *Intensity) {
  const uint8_t *y = src->buffers[0];
  const uint8_t *u = src->buffers[1];
  const uint8_t *v = src->buffers[2];

  const int y_height = src->crop_heights[0];
  const int y_width = src->crop_widths[0];
  const int y_stride = src->strides[0];
  const int c_stride = src->strides[1];

  for (int i = 0; i < y_height; i++) {
    for (int j = 0; j < y_width; j++) {
      Cr[i * y_width + j] =
          fclamp((double)y[i * y_stride + j] +
                     1.370 * (double)(v[(i >> subsampling_y) * c_stride +
                                        (j >> subsampling_x)] -
                                      128),
                 0, 255);
      Cg[i * y_width + j] =
          fclamp((double)y[i * y_stride + j] -
                     0.698 * (double)(u[(i >> subsampling_y) * c_stride +
                                        (j >> subsampling_x)] -
                                      128) -
                     0.337 * (double)(v[(i >> subsampling_y) * c_stride +
                                        (j >> subsampling_x)] -
                                      128),
                 0, 255);
      Cb[i * y_width + j] =
          fclamp((double)y[i * y_stride + j] +
                     1.732 * (double)(u[(i >> subsampling_y) * c_stride +
                                        (j >> subsampling_x)] -
                                      128),
                 0, 255);

      assert(Cr[i * y_width + j] >= 0 && Cr[i * y_width + j] <= 255);
      assert(Cg[i * y_width + j] >= 0 && Cg[i * y_width + j] <= 255);
      assert(Cb[i * y_width + j] >= 0 && Cb[i * y_width + j] <= 255);

      Intensity[i * y_width + j] =
          (double)(Cr[i * y_width + j] + Cg[i * y_width + j] +
                   Cb[i * y_width + j]) /
          3;
      assert(Intensity[i * y_width + j] >= 0 &&
             Intensity[i * y_width + j] <= 255);

      Intensity[i * y_width + j] /= 256;
      Cr[i * y_width + j] /= 256;
      Cg[i * y_width + j] /= 256;
      Cb[i * y_width + j] /= 256;
    }
  }
}

static INLINE double convolve_map(const double *filter, const double *map,
                                  const int size) {
  double result = 0;
  for (int i = 0; i < size; i++) {
    result += filter[i] * map[i];
  }
  return result;
}

// This function is to decimate the map by half, and apply Gaussian filter on
// top of the reduced map.
static INLINE void decimate_map(double *map, int height, int width, int stride,
                                double *reduced_map) {
  const int new_width = width / 2;
  const int window_size = 5;
  const double gaussian_filter[25] = {
    1. / 256, 1.0 / 64, 3. / 128, 1. / 64,  1. / 256, 1. / 64, 1. / 16,
    3. / 32,  1. / 16,  1. / 64,  3. / 128, 3. / 32,  9. / 64, 3. / 32,
    3. / 128, 1. / 64,  1. / 16,  3. / 32,  1. / 16,  1. / 64, 1. / 256,
    1. / 64,  3. / 128, 1. / 64,  1. / 256
  };

  double map_region[25];
  for (int y = 0; y < height - 1; y += 2) {
    for (int x = 0; x < width - 1; x += 2) {
      int i = 0;
      for (int yy = y - window_size / 2; yy <= y + window_size / 2; yy++) {
        for (int xx = x - window_size / 2; xx <= x + window_size / 2; xx++) {
          int yvalue = yy;
          int xvalue = xx;
          // copied values outside the boundary
          if (yvalue < 0) yvalue = 0;
          if (xvalue < 0) xvalue = 0;
          if (yvalue >= height) yvalue = height - 1;
          if (xvalue >= width) xvalue = width - 1;
          map_region[i++] = map[yvalue * stride + xvalue];
        }
      }
      reduced_map[(y / 2) * new_width + (x / 2)] = (double)convolve_map(
          gaussian_filter, map_region, window_size * window_size);
    }
  }
}

// This function is to upscale the map from in_level size to out_level size.
// Note that the map at "level-1" will upscale the map at "level" by x2.
static INLINE void upscale_map(double *input, int in_level, int out_level,
                               int height[9], int width[9], double *output) {
  for (int level = in_level; level > out_level; level--) {
    const int cur_width = width[level];
    const int cur_height = height[level];
    const int cur_stride = width[level];

    double *original =
        (double *)malloc(cur_width * cur_height * sizeof(double));

    if (level == in_level) {
      memcpy(original, input, cur_width * cur_height * sizeof(double));
    } else {
      memcpy(original, output, cur_width * cur_height * sizeof(double));
    }

    if (level > 0) {
      int h_upscale = height[level - 1];
      int w_upscale = width[level - 1];
      int s_upscale = width[level - 1];

      double *upscale =
          (double *)malloc(h_upscale * w_upscale * sizeof(double));

      int ii = 0;
      int jj = 0;

      for (int i = 0; i < h_upscale; ++i) {
        for (int j = 0; j < w_upscale; ++j) {
          ii = i / 2;
          jj = j / 2;

          if (jj >= cur_width) {
            jj = cur_width - 1;
          }
          if (ii >= cur_height) {
            ii = cur_height - 1;
          }

          upscale[j + i * s_upscale] = (double)original[jj + ii * cur_stride];
        }
      }
      memcpy(output, upscale, h_upscale * w_upscale * sizeof(double));
      free(upscale);
    }

    free(original);
  }
}

// This function calculates the differences between a fine scale c and a
// coarser scale s yielding the feature maps. c \in {2, 3, 4}, and s = c +
// delta, where delta \in {3, 4}.
static void Center_surround_diff(double *input[9], int height[9], int width[9],
                                 saliency_feature_map *output[6]) {
  int j = 0;
  for (int k = 2; k < 5; k++) {
    int cur_height = height[k];
    int cur_width = width[k];

    double *intermediate_map =
        (double *)malloc(cur_height * cur_width * sizeof(double));

    output[j]->buf = (double *)malloc(cur_height * cur_width * sizeof(double));
    output[j + 1]->buf =
        (double *)malloc(cur_height * cur_width * sizeof(double));

    output[j]->height = output[j + 1]->height = cur_height;
    output[j]->width = output[j + 1]->width = cur_width;

    upscale_map(input[k + 3], k + 3, k, height, width, intermediate_map);

    for (int r = 0; r < cur_height; r++) {
      for (int c = 0; c < cur_width; c++) {
        output[j]->buf[r * cur_width + c] =
            fabs((double)(input[k][r * cur_width + c] -
                          intermediate_map[r * cur_width + c]));
      }
    }

    upscale_map(input[k + 4], k + 4, k, height, width, intermediate_map);

    for (int r = 0; r < cur_height; r++) {
      for (int c = 0; c < cur_width; c++) {
        output[j + 1]->buf[r * cur_width + c] = fabs(
            input[k][r * cur_width + c] - intermediate_map[r * cur_width + c]);
      }
    }

    free(intermediate_map);
    j += 2;
  }
}

// For color channels, the differences is calculated based on "color
// double-opponency". For example, the RG feature map is constructed between a
// fine scale c of R-G component and a coarser scale s of G-R component.
static void Center_surround_diff_RGB(double *input_1[9], double *input_2[9],
                                     int height[9], int width[9],
                                     saliency_feature_map *output[6]) {
  int j = 0;
  for (int k = 2; k < 5; k++) {
    int cur_height = height[k];
    int cur_width = width[k];

    double *intermediate_map =
        (double *)malloc(cur_height * cur_width * sizeof(double));

    output[j]->buf = (double *)malloc(cur_height * cur_width * sizeof(double));
    output[j + 1]->buf =
        (double *)malloc(cur_height * cur_width * sizeof(double));

    output[j]->height = output[j + 1]->height = cur_height;
    output[j]->width = output[j + 1]->width = cur_width;

    upscale_map(input_2[k + 3], k + 3, k, height, width, intermediate_map);

    for (int r = 0; r < cur_height; r++) {
      for (int c = 0; c < cur_width; c++) {
        output[j]->buf[r * cur_width + c] =
            fabs((double)(input_1[k][r * cur_width + c] -
                          intermediate_map[r * cur_width + c]));
      }
    }

    upscale_map(input_2[k + 4], k + 4, k, height, width, intermediate_map);

    for (int r = 0; r < cur_height; r++) {
      for (int c = 0; c < cur_width; c++) {
        output[j + 1]->buf[r * cur_width + c] =
            fabs(input_1[k][r * cur_width + c] -
                 intermediate_map[r * cur_width + c]);
      }
    }

    free(intermediate_map);
    j += 2;
  }
}

// This function is to generate Gaussian pyramid images with indexes from 0 to
// 8, and construct the feature maps from calculating the center-surround
// differences.
static void Gaussian_Pyramid(double *src, int width, int height,
                             saliency_feature_map *dst[6]) {
  double *GaussianMap[9];  // scale = 9
  int pyr_width[9];
  int pyr_height[9];

  GaussianMap[0] = (double *)malloc(width * height * sizeof(double));
  memcpy(GaussianMap[0], src, width * height * sizeof(double));

  pyr_width[0] = width;
  pyr_height[0] = height;

  for (int i = 1; i < 9; i++) {
    int stride = pyr_width[i - 1];
    int new_width = pyr_width[i - 1] / 2;
    int new_height = pyr_height[i - 1] / 2;

    GaussianMap[i] = (double *)malloc(new_width * new_height * sizeof(double));
    memset(GaussianMap[i], 0, new_width * new_height * sizeof(double));

    decimate_map(GaussianMap[i - 1], pyr_height[i - 1], pyr_width[i - 1],
                 stride, GaussianMap[i]);

    pyr_width[i] = new_width;
    pyr_height[i] = new_height;
  }

  Center_surround_diff(GaussianMap, pyr_height, pyr_width, dst);

  for (int i = 0; i < 9; i++) {
    if (GaussianMap[i]) {
      free(GaussianMap[i]);
    }
  }
}

static void Gaussian_Pyramid_RGB(double *src_1, double *src_2, int width,
                                 int height, saliency_feature_map *dst[6]) {
  double *GaussianMap[2][9];  // scale = 9
  int pyr_width[9];
  int pyr_height[9];
  double *src[2];

  src[0] = src_1;
  src[1] = src_2;

  for (int k = 0; k < 2; k++) {
    GaussianMap[k][0] = (double *)malloc(width * height * sizeof(double));
    memcpy(GaussianMap[k][0], src[k], width * height * sizeof(double));

    pyr_width[0] = width;
    pyr_height[0] = height;

    for (int i = 1; i < 9; i++) {
      int stride = pyr_width[i - 1];
      int new_width = pyr_width[i - 1] / 2;
      int new_height = pyr_height[i - 1] / 2;

      GaussianMap[k][i] =
          (double *)malloc(new_width * new_height * sizeof(double));
      memset(GaussianMap[k][i], 0, new_width * new_height * sizeof(double));
      decimate_map(GaussianMap[k][i - 1], pyr_height[i - 1], pyr_width[i - 1],
                   stride, GaussianMap[k][i]);

      pyr_width[i] = new_width;
      pyr_height[i] = new_height;
    }
  }

  Center_surround_diff_RGB(GaussianMap[0], GaussianMap[1], pyr_height,
                           pyr_width, dst);

  for (int i = 0; i < 9; i++) {
    if (GaussianMap[0][i]) {
      free(GaussianMap[0][i]);
    }
    if (GaussianMap[1][i]) {
      free(GaussianMap[1][i]);
    }
  }
}

static void Get_Feature_Map_Intensity(double *Intensity, int width, int height,
                                      saliency_feature_map *I_map[6]) {
  Gaussian_Pyramid(Intensity, width, height, I_map);
}

static void Get_Feature_Map_RGB(double *Cr, double *Cg, double *Cb, int width,
                                int height, saliency_feature_map *RG_map[6],
                                saliency_feature_map *BY_map[6]) {
  double *R, *G, *B, *Y, *RGMat, *BYMat, *GRMat, *YBMat;

  R = (double *)malloc(height * width * sizeof(double));
  G = (double *)malloc(height * width * sizeof(double));
  B = (double *)malloc(height * width * sizeof(double));
  Y = (double *)malloc(height * width * sizeof(double));
  RGMat = (double *)malloc(height * width * sizeof(double));
  BYMat = (double *)malloc(height * width * sizeof(double));
  GRMat = (double *)malloc(height * width * sizeof(double));
  YBMat = (double *)malloc(height * width * sizeof(double));

  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      R[i * width + j] =
          Cr[i * width + j] - (Cg[i * width + j] + Cb[i * width + j]) / 2;
      G[i * width + j] =
          Cg[i * width + j] - (Cr[i * width + j] + Cb[i * width + j]) / 2;
      B[i * width + j] =
          Cb[i * width + j] - (Cr[i * width + j] + Cg[i * width + j]) / 2;
      Y[i * width + j] = (Cr[i * width + j] + Cg[i * width + j]) / 2 -
                         fabs(Cr[i * width + j] - Cg[i * width + j]) / 2 -
                         Cb[i * width + j];

      R[i * width + j] = AOMMAX(0, R[i * width + j]);
      G[i * width + j] = AOMMAX(0, G[i * width + j]);
      B[i * width + j] = AOMMAX(0, B[i * width + j]);
      Y[i * width + j] = AOMMAX(0, Y[i * width + j]);

      RGMat[i * width + j] = R[i * width + j] - G[i * width + j];
      BYMat[i * width + j] = B[i * width + j] - Y[i * width + j];
      GRMat[i * width + j] = G[i * width + j] - R[i * width + j];
      YBMat[i * width + j] = Y[i * width + j] - B[i * width + j];
    }
  }

  Gaussian_Pyramid_RGB(RGMat, GRMat, width, height, RG_map);
  Gaussian_Pyramid_RGB(BYMat, YBMat, width, height, BY_map);

  free(R);
  free(G);
  free(B);
  free(Y);
  free(RGMat);
  free(BYMat);
  free(GRMat);
  free(YBMat);
}

static INLINE void Filter2D(double *input, const double kernel[9][9], int width,
                            int height, double *output) {
  const int window_size = 9;
  double img_section[81];
  for (int y = 0; y <= height - 1; y++) {
    for (int x = 0; x <= width - 1; x++) {
      int i = 0;
      for (int yy = y - window_size / 2; yy <= y + window_size / 2; yy++) {
        for (int xx = x - window_size / 2; xx <= x + window_size / 2; xx++) {
          int yvalue = yy;
          int xvalue = xx;
          // copied pixels outside the boundary
          if (yvalue < 0) yvalue = 0;
          if (xvalue < 0) xvalue = 0;
          if (yvalue >= height) yvalue = height - 1;
          if (xvalue >= width) xvalue = width - 1;
          img_section[i++] = input[yvalue * width + xvalue];
        }
      }

      output[y * width + x] = 0;
      for (int k = 0; k < window_size; k++) {
        for (int l = 0; l < window_size; l++) {
          output[y * width + x] +=
              kernel[k][l] * img_section[k * window_size + l];
        }
      }
    }
  }
}

static void Get_Feature_Map_Orientation(double *Intensity, int width,
                                        int height,
                                        saliency_feature_map *dst[24]) {
  double *GaussianMap[9];
  int pyr_width[9];
  int pyr_height[9];

  GaussianMap[0] = (double *)malloc(width * height * sizeof(double));
  memcpy(GaussianMap[0], Intensity, width * height * sizeof(double));

  pyr_width[0] = width;
  pyr_height[0] = height;

  for (int i = 1; i < 9; i++) {
    int stride = pyr_width[i - 1];
    int new_width = pyr_width[i - 1] / 2;
    int new_height = pyr_height[i - 1] / 2;

    GaussianMap[i] = (double *)malloc(new_width * new_height * sizeof(double));
    memset(GaussianMap[i], 0, new_width * new_height * sizeof(double));
    decimate_map(GaussianMap[i - 1], pyr_height[i - 1], pyr_width[i - 1],
                 stride, GaussianMap[i]);

    pyr_width[i] = new_width;
    pyr_height[i] = new_height;
  }

  double *tempGaborOutput0[9], *tempGaborOutput45[9], *tempGaborOutput90[9],
      *tempGaborOutput135[9];

  for (int i = 2; i < 9; i++) {
    const int cur_height = pyr_height[i];
    const int cur_width = pyr_width[i];
    tempGaborOutput0[i] =
        (double *)malloc(cur_height * cur_width * sizeof(double));
    tempGaborOutput45[i] =
        (double *)malloc(cur_height * cur_width * sizeof(double));
    tempGaborOutput90[i] =
        (double *)malloc(cur_height * cur_width * sizeof(double));
    tempGaborOutput135[i] =
        (double *)malloc(cur_height * cur_width * sizeof(double));
    Filter2D(GaussianMap[i], kGaborFilter[0], cur_width, cur_height,
             tempGaborOutput0[i]);
    Filter2D(GaussianMap[i], kGaborFilter[1], cur_width, cur_height,
             tempGaborOutput45[i]);
    Filter2D(GaussianMap[i], kGaborFilter[2], cur_width, cur_height,
             tempGaborOutput90[i]);
    Filter2D(GaussianMap[i], kGaborFilter[3], cur_width, cur_height,
             tempGaborOutput135[i]);
  }

  for (int i = 0; i < 9; i++) {
    if (GaussianMap[i]) {
      free(GaussianMap[i]);
    }
  }

  saliency_feature_map *tmp0[6], *tmp45[6], *tmp90[6], *tmp135[6];

  for (int i = 0; i < 6; i++) {
    tmp0[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
    tmp45[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
    tmp90[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
    tmp135[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  }

  Center_surround_diff(tempGaborOutput0, pyr_height, pyr_width, tmp0);
  Center_surround_diff(tempGaborOutput45, pyr_height, pyr_width, tmp45);
  Center_surround_diff(tempGaborOutput90, pyr_height, pyr_width, tmp90);
  Center_surround_diff(tempGaborOutput135, pyr_height, pyr_width, tmp135);

  for (int i = 2; i < 9; i++) {
    free(tempGaborOutput0[i]);
    free(tempGaborOutput45[i]);
    free(tempGaborOutput90[i]);
    free(tempGaborOutput135[i]);
  }

  for (int i = 0; i < 6; i++) {
    dst[i] = tmp0[i];
    dst[i + 6] = tmp45[i];
    dst[i + 12] = tmp90[i];
    dst[i + 18] = tmp135[i];
  }
}

static INLINE void FindMinMax(saliency_feature_map *input, double *max_value,
                              double *min_value) {
  assert(input && input->buf);
  *min_value = 1;
  *max_value = 0;

  for (int i = 0; i < input->height; i++) {
    for (int j = 0; j < input->width; j++) {
      *min_value = fmin(input->buf[i * input->width + j], *min_value);
      *max_value = fmax(input->buf[i * input->width + j], *max_value);
    }
  }
}

static INLINE double AverageLocalMax(saliency_feature_map *input,
                                     int stepsize) {
  int numlocal = 0;
  double lmaxmean = 0, lmax = 0, dummy = 0;
  saliency_feature_map localMap;
  localMap.height = stepsize;
  localMap.width = stepsize;
  localMap.buf = (double *)malloc(stepsize * stepsize * sizeof(double));

  for (int y = 0; y < input->height - stepsize; y += stepsize) {
    for (int x = 0; x < input->width - stepsize; x += stepsize) {
      for (int i = 0; i < stepsize; i++) {
        for (int j = 0; j < stepsize; j++) {
          localMap.buf[i * stepsize + j] =
              input->buf[(y + i) * input->width + x + j];
        }
      }

      FindMinMax(&localMap, &lmax, &dummy);
      lmaxmean += lmax;
      numlocal++;
    }
  }

  free(localMap.buf);

  return lmaxmean / numlocal;
}

// Linear normalization the values in the map to [0,1].
static saliency_feature_map *MinMaxNormalize(saliency_feature_map *input) {
  double maxx, minn;

  FindMinMax(input, &maxx, &minn);

  saliency_feature_map *result =
      (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  result->buf = (double *)malloc(input->width * input->height * sizeof(double));
  result->height = input->height;
  result->width = input->width;
  memset(result->buf, 0, input->width * input->height * sizeof(double));

  for (int i = 0; i < input->height; i++) {
    for (int j = 0; j < input->width; j++) {
      if (maxx != minn) {
        result->buf[i * input->width + j] =
            input->buf[i * input->width + j] / (maxx - minn) +
            minn / (minn - maxx);
      } else {
        result->buf[i * input->width + j] =
            input->buf[i * input->width + j] - minn;
      }
    }
  }

  return result;
}

// This function is to promote meaningful “activation spots” in the map and
// ignores homogeneous areas.
static saliency_feature_map *Nomalization_Operator(saliency_feature_map *input,
                                                   int stepsize,
                                                   bool free_input) {
  saliency_feature_map *result =
      (saliency_feature_map *)malloc(sizeof(saliency_feature_map));

  result->buf = (double *)malloc(input->width * input->height * sizeof(double));
  result->height = input->height;
  result->width = input->width;

  saliency_feature_map *tempResult = MinMaxNormalize(input);

  double lmaxmean = AverageLocalMax(tempResult, stepsize);
  double normCoeff = (1 - lmaxmean) * (1 - lmaxmean);

  for (int i = 0; i < input->height; i++) {
    for (int j = 0; j < input->width; j++) {
      result->buf[i * input->width + j] =
          tempResult->buf[i * input->width + j] * normCoeff;
    }
  }

  free(tempResult->buf);
  free(tempResult);

  if (free_input) {
    free(input->buf);
    free(input);
  }

  return result;
}

// Normalize the values in feature maps to [0,1], and then upscale all maps to
// the original frame size.
static void normalizeFM(saliency_feature_map *input[6], int width, int height,
                        int num_FM, saliency_feature_map *output[6]) {
  int pyr_height[9];
  int pyr_width[9];

  pyr_height[0] = height;
  pyr_width[0] = width;

  for (int i = 1; i < 9; i++) {
    int new_width = pyr_width[i - 1] / 2;
    int new_height = pyr_height[i - 1] / 2;

    pyr_width[i] = new_width;
    pyr_height[i] = new_height;
  }

  double *tmp = (double *)malloc(width * height * sizeof(double));

  // Feature maps (FM) are generated by function "Center_surround_diff()". The
  // difference is between a fine scale c and a coarser scale s, where c \in {2,
  // 3, 4}, and s = c + delta, where delta \in {3, 4}, and the FM size is scale
  // c. Specifically, i=0: c=2 and s=5, i=1: c=2 and s=6, i=2: c=3 and s=6, i=3:
  // c=3 and s=7, i=4: c=4 and s=7, i=5: c=4 and s=8.
  for (int i = 0; i < num_FM; i++) {
    // Normalization
    saliency_feature_map *normalizedmatrix =
        Nomalization_Operator(input[i], 8, false);
    output[i]->buf = (double *)malloc(width * height * sizeof(double));
    output[i]->height = height;
    output[i]->width = width;
    // Upscale FM to original frame size
    upscale_map(normalizedmatrix->buf, (i / 2) + 2, 0, pyr_height, pyr_width,
                tmp);

    memcpy(output[i]->buf, (double *)tmp, width * height * sizeof(double));
    free(normalizedmatrix->buf);
  }
  free(tmp);
}

// Combine feature maps with the same category (intensity, color, or
// orientation) into one conspicuity map.
static saliency_feature_map *NormalizedMap(saliency_feature_map *input[6],
                                           int width, int height) {
  int num_FM = 6;

  saliency_feature_map *Ninput[6];
  for (int i = 0; i < 6; i++) {
    Ninput[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  }
  normalizeFM(input, width, height, num_FM, Ninput);

  saliency_feature_map *output =
      (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  output->buf = (double *)malloc(width * height * sizeof(double));
  output->height = height;
  output->width = width;
  memset(output->buf, 0, width * height * sizeof(double));

  // Add up all normalized feature maps with the same category into one map.
  for (int r = 0; r < height; r++) {
    for (int c = 0; c < width; c++) {
      for (int i = 0; i < num_FM; i++) {
        output->buf[r * width + c] += Ninput[i]->buf[r * width + c];
      }
    }
  }

  for (int i = 0; i < num_FM; i++) {
    free(Ninput[i]->buf);
    free(Ninput[i]);
  }

  return Nomalization_Operator(output, 8, true);
}

static saliency_feature_map *NormalizedMap_RGB(saliency_feature_map *RG_map[6],
                                               saliency_feature_map *BY_map[6],
                                               int width, int height) {
  saliency_feature_map *CCM_RG = NormalizedMap(RG_map, width, height);
  saliency_feature_map *CCM_BY = NormalizedMap(BY_map, width, height);

  saliency_feature_map *CCM =
      (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  CCM->buf = (double *)malloc(width * height * sizeof(double));
  CCM->height = height;
  CCM->width = width;

  for (int r = 0; r < height; r++) {
    for (int c = 0; c < width; c++) {
      CCM->buf[r * width + c] =
          CCM_RG->buf[r * width + c] + CCM_BY->buf[r * width + c];
    }
  }

  free(CCM_RG->buf);
  free(CCM_BY->buf);
  free(CCM_RG);
  free(CCM_BY);

  return Nomalization_Operator(CCM, 8, true);
}

static saliency_feature_map *NormalizedMap_Orientation(
    saliency_feature_map *Orientation_map[24], int width, int height) {
  int num_FMs_perAngle = 6;

  saliency_feature_map *OFM0[6];
  saliency_feature_map *OFM45[6];
  saliency_feature_map *OFM90[6];
  saliency_feature_map *OFM135[6];

  for (int i = 0; i < num_FMs_perAngle; i++) {
    OFM0[i] = Orientation_map[0 * num_FMs_perAngle + i];
    OFM45[i] = Orientation_map[1 * num_FMs_perAngle + i];
    OFM90[i] = Orientation_map[2 * num_FMs_perAngle + i];
    OFM135[i] = Orientation_map[3 * num_FMs_perAngle + i];
  }

  // extract conspicuity map for each angle
  saliency_feature_map *NOFM[4];
  NOFM[0] = NormalizedMap(OFM0, width, height);
  NOFM[1] = NormalizedMap(OFM45, width, height);
  NOFM[2] = NormalizedMap(OFM90, width, height);
  NOFM[3] = NormalizedMap(OFM135, width, height);

  for (int i = 0; i < 4; i++) {
    NOFM[i]->height = height;
    NOFM[i]->width = width;
  }

  saliency_feature_map *OCM =
      (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  OCM->buf = (double *)malloc(width * height * sizeof(double));
  OCM->height = height;
  OCM->width = width;
  memset(OCM->buf, 0, width * height * sizeof(double));

  for (int i = 0; i < 4; i++) {
    for (int r = 0; r < height; r++) {
      for (int c = 0; c < width; c++) {
        OCM->buf[r * width + c] += NOFM[i]->buf[r * width + c];
      }
    }
    free(NOFM[i]->buf);
    free(NOFM[i]);
  }

  return Nomalization_Operator(OCM, 8, true);
}

// Set pixel level saliency mask based on Itti-Koch algorithm
void av1_set_saliency_map(AV1_COMP *cpi) {
  AV1_COMMON *const cm = &cpi->common;

  int frm_width = cm->width;
  int frm_height = cm->height;

  double *Cr = (double *)malloc(frm_width * frm_height * sizeof(double));
  double *Cg = (double *)malloc(frm_width * frm_height * sizeof(double));
  double *Cb = (double *)malloc(frm_width * frm_height * sizeof(double));
  double *Intensity = (double *)malloc(frm_width * frm_height * sizeof(double));

  // Extract red / green / blue channels and Intensity component
  get_color_intensity(cpi->source, cm->seq_params->subsampling_x,
                      cm->seq_params->subsampling_y, Cr, Cg, Cb, Intensity);

  // Feature Map Extraction
  // Intensity map
  saliency_feature_map *I_map[6];
  for (int i = 0; i < 6; i++) {
    I_map[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  }

  Get_Feature_Map_Intensity(Intensity, frm_width, frm_height, I_map);

  // RGB map
  saliency_feature_map *RG_map[6], *BY_map[6];
  for (int i = 0; i < 6; i++) {
    RG_map[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
    BY_map[i] = (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  }
  Get_Feature_Map_RGB(Cr, Cg, Cb, frm_width, frm_height, RG_map, BY_map);

  // Orientation map
  saliency_feature_map *Orientation_map[24];
  for (int i = 0; i < 24; i++) {
    Orientation_map[i] =
        (saliency_feature_map *)malloc(sizeof(saliency_feature_map));
  }
  Get_Feature_Map_Orientation(Intensity, frm_width, frm_height,
                              Orientation_map);

  free(Cr);
  free(Cg);
  free(Cb);
  free(Intensity);

  // Conspicuity map generation
  saliency_feature_map *INM = NormalizedMap(I_map, frm_width, frm_height);
  saliency_feature_map *CNM =
      NormalizedMap_RGB(RG_map, BY_map, frm_width, frm_height);
  saliency_feature_map *ONM =
      NormalizedMap_Orientation(Orientation_map, frm_width, frm_height);

  for (int i = 0; i < 6; i++) {
    free(I_map[i]->buf);
    free(RG_map[i]->buf);
    free(BY_map[i]->buf);
    free(I_map[i]);
    free(RG_map[i]);
    free(BY_map[i]);
  }

  for (int i = 0; i < 24; i++) {
    free(Orientation_map[i]->buf);
    free(Orientation_map[i]);
  }

  // Pixel level saliency map
  saliency_feature_map *Saliency_Map =
      (saliency_feature_map *)malloc(sizeof(saliency_feature_map));

  Saliency_Map->buf = (double *)malloc(frm_width * frm_height * sizeof(double));
  Saliency_Map->height = frm_height;
  Saliency_Map->width = frm_width;

  double w_intensity, w_color, w_orient;

  w_intensity = w_color = w_orient = (double)1 / 3;

  for (int r = 0; r < frm_height; r++) {
    for (int c = 0; c < frm_width; c++) {
      Saliency_Map->buf[r * frm_width + c] =
          (w_intensity * INM->buf[r * frm_width + c] +
           w_color * CNM->buf[r * frm_width + c] +
           w_orient * ONM->buf[r * frm_width + c]);
    }
  }

  Saliency_Map = MinMaxNormalize(Saliency_Map);  // Normalization

  memcpy(cpi->saliency_map, Saliency_Map->buf,
         frm_width * frm_height * sizeof(double));

  free(INM->buf);
  free(INM);

  free(CNM->buf);
  free(CNM);

  free(ONM->buf);
  free(ONM);

  free(Saliency_Map->buf);
  free(Saliency_Map);
}
